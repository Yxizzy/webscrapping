import requests #make Get request to the server
from requests.exceptions import RequestException
from bs4 import BeautifulSoup 
# from time import sleep
# import sys
import re

def get_url():
    urls = ['https://www.nairaland.com/']
    for url in urls:
        print(url)
        page = requests.get(url)
        soup = BeautifulSoup(page.content, 'html.parser')

        for link in soup.find_all('a'):
            links = link.get('href')
            if type(links) == str:
                main = links
                
                if 'http' in main:
                    mains = main
                    found_url = []
                    try:
                        pages = requests.get(mains)
                        searched_word = "the"
                        soup = BeautifulSoup(pages.content, 'html.parser')
                        results = soup.body.find_all(string=re.compile('.*{0}.*'.format(searched_word)), recursive=True)

                        print ('Found the word "{0}" {1} times\n'.format(searched_word, len(results)))
                        matching = [s for s in results if "the" in s]
                        print(mains)
                        
                    except RequestException as e:
                        print(e)
                   
get_url()                  
